import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:athlon_task_atanas/navigation/routing.dart';
import 'package:athlon_task_atanas/navigation/routing_constants.dart';
import 'package:athlon_task_atanas/states/auth_state.dart';
import 'package:athlon_task_atanas/ui/control_widget.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: AuthState(),
      child: const MaterialApp(
        onGenerateRoute: Routing.generateRoute,
        initialRoute: RoutingConst.defaultRoute,
        title: 'Athlon Task',
        home: ControlWidget(),
      ),
    );
  }
}
