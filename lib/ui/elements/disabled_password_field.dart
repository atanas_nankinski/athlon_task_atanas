import 'package:flutter/material.dart';

import 'package:athlon_task_atanas/ui/styles/border_decorations.dart';

/// Widget for disabled password text field
///
/// This widget is only used in the settings screen.
class DisabledPasswordInput extends StatefulWidget {
  const DisabledPasswordInput({Key? key}) : super(key: key);

  @override
  State<DisabledPasswordInput> createState() => _DisabledPasswordInputState();
}

class _DisabledPasswordInputState extends State<DisabledPasswordInput> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            margin: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 5.0),
            child: const Text(
              "Password",
              style: TextStyle(
                  fontSize: 12
              ),
            )
        ),
        Container(
          child: TextFormField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            obscureText: true,
            enableSuggestions: false,
            autocorrect: false,
            enabled: false,
            initialValue: "password",
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.grey.shade300,
              contentPadding: const EdgeInsets.fromLTRB(15.0, 8.0, 15.0, 8.0),
                disabledBorder: BorderDecorations().borderDecor()
            ),
            style: const TextStyle(
                fontSize: 14
            ),
          ),
        ),
      ],
    );
  }
}