import 'package:flutter/material.dart';

/// Utility widget used containing divider and empty space
///
/// This widget is so far implemented only in the login screen
class LoginDivider extends StatelessWidget {
  const LoginDivider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        const Expanded(
          child: Divider(
            thickness: 1,
            endIndent: 5,
          ),
        ),
        Text(
          "or",
          style: TextStyle(
              color: Colors.grey.shade600
          ),
        ),
        const Expanded(
          child: Divider(
            indent: 5,
            thickness: 1,
          ),
        ),
      ],
    );
  }
}
