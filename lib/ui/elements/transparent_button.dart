import 'package:flutter/material.dart';

/// Widget holding the button that can be reused
///
/// So far this button is only used for the preview of the settings screen
/// when the user is not logged in.
class TransparentButton extends StatelessWidget {
  const TransparentButton({Key? key, required this.btnPress}) : super(key: key);
  final void Function() btnPress;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 40,
      margin: EdgeInsets.only(top: 20),
      child: ElevatedButton(
        onPressed: btnPress,
        child: Text(
          "Explore The App",
          style: TextStyle(
              color: Colors.grey.shade600
          ),
        ),
        style: ElevatedButton.styleFrom(
          primary: Colors.white,
          elevation: 0.0,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: Colors.grey.shade600,
            ),
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
