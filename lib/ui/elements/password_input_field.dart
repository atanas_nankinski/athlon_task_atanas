import 'package:flutter/material.dart';

import 'package:athlon_task_atanas/ui/styles/border_decorations.dart';

/// This widget is for the password text field.
class PasswordInputField extends StatefulWidget {
  const PasswordInputField({Key? key, required this.controller}) : super(key: key);
  final TextEditingController controller;

  @override
  State<PasswordInputField> createState() => _PasswordInputFieldState();
}

class _PasswordInputFieldState extends State<PasswordInputField> {
  //Bool value used for determening if the text in the field should be obscured
  bool _isObscured = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            margin: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 5.0),
            child: const Text(
              "Password",
              style: TextStyle(
                fontSize: 12
              ),
            )
        ),
        Container(
          child: TextFormField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            controller: widget.controller,
            obscureText: _isObscured,
            enableSuggestions: false,
            autocorrect: false,
            decoration: InputDecoration(
              hintText: "enter password",
              contentPadding: const EdgeInsets.fromLTRB(15.0, 8.0, 15.0, 8.0),
              enabledBorder: BorderDecorations().borderDecor(),
              border: BorderDecorations().borderDecor(),
              suffixIcon: IconButton(
                icon: Icon(
                  // Checking which icon should be used based on if the text is obscured or not
                  _isObscured ? Icons.visibility_off_outlined : Icons.visibility_outlined,
                ),
                iconSize: 20,
                onPressed: iconPress,
              ),
            ),
            style: const TextStyle(
                fontSize: 14
            ),
          ),
        ),
      ],
    );
  }

  /// Pressed event for the icons inside the text field.
  void iconPress(){
    setState(() {
      _isObscured = !_isObscured;
    });
  }
}
