import 'package:flutter/material.dart';

import 'package:athlon_task_atanas/ui/styles/border_decorations.dart';


/// Reusable widget for the email text field
class EmailInputField extends StatelessWidget {
  const EmailInputField({Key? key, required this.controller, required this.isFieldEnabled}) : super(key: key);
  final TextEditingController controller;
  // Bool value used to determine if the field should be with disabled style
  final bool isFieldEnabled;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 5.0),
            child: const Text(
              "Email",
              style: TextStyle(
                  fontSize: 12
              ),
            )
        ),
        Container(
          child: TextFormField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: (value) => _validateEmail(value),
            controller: controller,
            enabled: isFieldEnabled,
            decoration: InputDecoration(
              filled: !isFieldEnabled,
              fillColor: Colors.grey.shade300,
              hintText: "e.g johndoe@mail.com",
              contentPadding: const EdgeInsets.fromLTRB(15.0, 8.0, 8.0, 10.0),
              enabledBorder: BorderDecorations().borderDecor(),
              disabledBorder: BorderDecorations().borderDecor(),
              border: BorderDecorations().borderDecor(),
            ),
            style: const TextStyle(
              fontSize: 14
            ),
          ),
        ),
      ],
    );
  }

  /// Method used for validating if the text in the field is in the correct email format
  String? _validateEmail(String? value) {
    String pattern =
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?)*$";
    RegExp regex = RegExp(pattern);
    if (value == null || value.isEmpty || !regex.hasMatch(value)){
      return 'Enter a valid email address';
    }
    return null;
  }
}
