import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:athlon_task_atanas/ui/settings_page/profile_info.dart';
import 'package:athlon_task_atanas/ui/elements/email_input_field.dart';
import 'package:athlon_task_atanas/models/user_model.dart';
import 'package:athlon_task_atanas/navigation/routing_constants.dart';
import 'package:athlon_task_atanas/states/auth_state.dart';
import 'package:athlon_task_atanas/states/profile_state.dart';
import 'package:athlon_task_atanas/ui/elements/disabled_password_field.dart';

///Widget holding the settings screen for both users that are not logged in and that are logged
class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  User? user;

  @override
  Widget build(BuildContext context) {
    user = Provider.of<AuthState>(context).user;
    //Declaring controller for the email text field,
    //this controller is used mostly for setting initial text
    TextEditingController emailController = TextEditingController(text: _isLogged(user) ? user?.email : "");

    //Hiding the snackbar due to it appearing even if the user logged in successfully
    ScaffoldMessenger.of(context).hideCurrentSnackBar();

    getProfile(user);

    return Scaffold(
      resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: const Text("Settings"),
          backgroundColor: Colors.indigo,
          automaticallyImplyLeading: false,
        ),
        body: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Checking if the user is logged in order to display either profile info or the text widget
              _isLogged(user) ? const ProfileInfo()
                  : Container(
                    margin: const EdgeInsets.fromLTRB(0.0, 30.0, 0.0, 30.0),
                    child: const Center(
                      child: Text(
                        "You're not logged in",
                        style: TextStyle(
                          fontSize: 30,
                        ),
                      ),
                    ),
                  ),
              EmailInputField(controller: emailController, isFieldEnabled: _isLogged(user)),
              const DisabledPasswordInput(),
              Container(
                margin: const EdgeInsets.only(top: 10.0),
                child: RichText(
                  text: const TextSpan(
                    text: "Change",
                    style: TextStyle(
                        color: Colors.blue,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
              const Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const Spacer(),
                  Expanded(
                    child: Container(
                      //Checking if the user is logged in order to display either login or logg off button
                      child: _isLogged(user) ? ElevatedButton(
                        onPressed: () => _btnLogOffPress(context),
                        child: const Text("Log off"),
                      ) : ElevatedButton(
                        onPressed: _btnLoginPress,
                        child: const Text("Login"),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
    );
  }

  /// Pressed event for the logg off button
  ///
  /// This event calls the singOut method of firebase in order to sign out the user,
  /// then it will automatically navigate to the login screen due to the user value
  /// becoming null
  void _btnLogOffPress(BuildContext context){
    final authState = Provider.of<AuthState>(context, listen: false);
    authState.signOut();
  }

  /// Pressed event for the login button
  ///
  /// This event navigates back to the login screen.
  void _btnLoginPress(){
    Navigator.pushNamed(context, RoutingConst.defaultRoute);
  }

  /// Method returning bool value checking if the user is logged in
  bool _isLogged(User? userData){
    if(userData == null){
      return false;
    }
    return true;
  }

  /// This method gets the profile data for the settings screen.
  ///
  /// The method calls the ProfileService via the profile data provider.
  void getProfile(User? user) {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      if (mounted) {
        final profileState = Provider.of<ProfileState>(context, listen: false);
        profileState.getProfileData(user?.uid ?? "");
      }
    });
  }
}