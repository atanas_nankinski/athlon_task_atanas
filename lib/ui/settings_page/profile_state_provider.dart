import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:athlon_task_atanas/states/profile_state.dart';

/// Widget providing the ProfileState provider to the child widget assigned to it
class ProfileStateProvider extends StatelessWidget {
  final Widget child;
  const ProfileStateProvider({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: ProfileState(),
      child: child,
    );
  }
}