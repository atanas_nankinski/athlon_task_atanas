import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:athlon_task_atanas/states/profile_state.dart';

/// Widget that is being called inside the settings screen when user is logged in.
///
/// This widget provides the profile data inside the settings screen when the user is logged in.
class ProfileInfo extends StatelessWidget {
  const ProfileInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //Default image url used if the user doesn't have avatar
    String defaultUrl = "https://st3.depositphotos.com/1767687/16607/v/600/depositphotos_166074422-stock-illustration-default-avatar-profile-icon-grey.jpg";

    //Calling the ProfileState provider
    final profileData = Provider.of<ProfileState>(context).profileData;

    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Avatar",
            style: TextStyle(
              color: Colors.grey[600],
              fontSize: 12,
            ),
          ),
          const SizedBox(height: 20),
          Row(
            children: [
              CircleAvatar(
                radius: 30.0,
                backgroundImage: NetworkImage(profileData?.avatarUrl ?? defaultUrl),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _getImageName(profileData?.avatarUrl),
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    const SizedBox(height: 10),
                    Text(
                      "Uploaded " + _getFormatedDate(profileData?.updatedAt ?? ""),
                      style: TextStyle(
                        fontSize: 10,
                        color: Colors.grey[600],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 16),
        ],
      ),
    );
  }

  /// Method taking the image file name
  ///
  /// This method uses the avatar URL taken from the profile service
  /// and removes everything in the string of the url except the file name
  String _getImageName(String? link) {
    if(link != null) {
      String name = link.substring(link.lastIndexOf("/") + 1);

      return name;
    }
    return "";
  }

  /// Method reformating the date.
  ///
  /// This method takes the date from the ProfileService and formates it
  /// in the desired for the ui format.
  String _getFormatedDate(String date){
    if(date.isNotEmpty){
      final DateTime dateTime = DateTime.parse(date);
      final newDate = DateFormat.yMMMd().format(dateTime);

      return newDate;
    }
    return date;
  }
}
