import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:athlon_task_atanas/models/user_model.dart';
import 'package:athlon_task_atanas/ui/settings_page/profile_state_provider.dart';
import 'package:athlon_task_atanas/states/auth_state.dart';
import 'package:athlon_task_atanas/ui/login_screen.dart';
import 'package:athlon_task_atanas/ui/settings_page/settings_screen.dart';

///This is master widget controlling which screen the user
///
/// What screen is visible to the user depends on if the user
/// is logged in or not.
class ControlWidget extends StatelessWidget {
  const ControlWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authState = Provider.of<AuthState>(context);
    final user = authState.authStream;

    //The StreamBuilder is used to ensure we are checking if the user is logged or not,
    //preventing the authentication state of the user inside the app to change on rebuild
    return StreamBuilder(
      stream: user,
      builder: (context, AsyncSnapshot<User?> snapshot) {
        //This boolean is used to check if the user is logged in order to display the proper screen
        final bool hasUser = snapshot.hasData && (snapshot.data!.uid ?? "").isNotEmpty;
        final user = snapshot.data;
        authState.user = user;

        //Simple AnimatedSwitcher creating smoother transition when navigating via the master widget
        return AnimatedSwitcher(
          duration: const Duration(milliseconds: 200),
          child: hasUser
              ? const ProfileStateProvider(child: SettingsScreen())
              : const LoginScreen(),
        );
      },
    );
  }
}
