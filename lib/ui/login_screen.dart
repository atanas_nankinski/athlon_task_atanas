import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:athlon_task_atanas/ui/elements/email_input_field.dart';
import 'package:athlon_task_atanas/ui/elements/password_input_field.dart';
import 'package:athlon_task_atanas/navigation/routing_constants.dart';
import 'package:athlon_task_atanas/states/auth_state.dart';
import 'package:athlon_task_atanas/ui/elements/login_divider.dart';
import 'package:athlon_task_atanas/ui/elements/transparent_button.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  //Declaring util values helping us manage some widgets/elements
  bool isFieldEmpty = false;
  bool isLoginPressed = false;
  //Declaring the text field controllers
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final authState = Provider.of<AuthState>(context, listen: false);

    //Calling the method controlling the SnackBar display,
    //we are calling it in the build method due to using information
    //from the auth service that can be initially null or empty.
    _showSnackBar(context, authState.errorMessage);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 30.0),
                  child: const Text(
                    "Welcome",
                    style: TextStyle(
                      fontSize: 30,
                    ),
                  ),
                ),
                EmailInputField(controller: _emailController, isFieldEnabled: true),
                PasswordInputField(controller: _passwordController),
                Container(
                  width: double.infinity,
                  height: 40,
                  margin: const EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 10.0),
                  child: ElevatedButton(
                    onPressed: () => _btnLoginPress(authState),
                    child: const Text("Login"),
                    style: ElevatedButton.styleFrom(
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ),
                ),
                TextButton(
                  onPressed: forgotPasswordClick,
                  child: const Text("Forgot password"),
                ),
                const LoginDivider(),
                TransparentButton(btnPress: _btnViewAppPress)
              ],
            ),
          ),
        ),
      ),
    );
  }

  ///Pressed event for the login button.
  ///
  /// This methods calls the Authentication service via the provider
  /// and if the text field input is correct it checks if the user can logg in.
  void _btnLoginPress(AuthState authState){
    isLoginPressed = true;
    if(_emailController.text.isNotEmpty && _passwordController.text.isNotEmpty){
      authState.signIn(_emailController.text, _passwordController.text);
      setState(() {
        isFieldEmpty = false;
      });
    }else {
      setState(() {
        isFieldEmpty = true;
      });
    }
  }

  ///Function displaying the SnackBar on failed login
  void _showSnackBar(BuildContext context, String msg){
    //Preventing setState being called before widget initialization error
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      //Checking if the login btn is being pressed atleast once
      //so the toast doesn't display on the build
      if(isLoginPressed){
        if(isFieldEmpty){
          msg = "There are empty fields!";
        }else {
          if(msg == ""){
            msg = "Invalid data!";
          }
        }

        final snackBar = SnackBar(
          content: Text(
            msg,
            textAlign: TextAlign.center,
          ),
          duration: Duration(seconds: 2),
          shape: StadiumBorder(),
          behavior: SnackBarBehavior.floating,
        );

        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        isLoginPressed = false;
      }
    });
  }

  ///Pressed event navigating to the Settings Screen when the user isn't logged in
  void _btnViewAppPress(){
    Navigator.pushNamed(context, RoutingConst.viewAppRoute);
  }

  ///Pressed event for the Forgot Password text button
  ///
  /// For now the event doesn't have any practical functionallity due to the
  /// feature not being implemented.
  void forgotPasswordClick(){
    print("Forgot Password");
  }
}
