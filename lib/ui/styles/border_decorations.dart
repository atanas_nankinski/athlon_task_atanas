import 'package:flutter/material.dart';

///This class holds the style for text field border.
///
/// This style is used by most of the text fields in the app.
class BorderDecorations {
  OutlineInputBorder borderDecor(){
    return OutlineInputBorder(
      borderSide: BorderSide(color: Colors.grey.shade400, style: BorderStyle.solid),
      borderRadius: BorderRadius.circular(10.0),
    );
  }
}