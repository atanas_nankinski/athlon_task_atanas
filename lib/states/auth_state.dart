import 'package:flutter/material.dart';

import 'package:athlon_task_atanas/models/user_model.dart';
import 'package:athlon_task_atanas/services/authentication_service.dart';


/// State class for the Authentication provider.
///
/// This class is calling the Authentication Service in order to pass the auth data
/// from the service to the ui. Also provider Stream to the ui to check if the user is still logged.
class AuthState with ChangeNotifier{
  final _authService = AuthService();
  User? _user;
  //Error message string passed to the ui for the SnackBar in the login screen.
  String _errorMessage = "";

  User? get user => _user;

  set user(User? user) {
    _user = user;
  }

  String get errorMessage => _errorMessage;

  /// Stream of User passed to the ui in order to check the user session.
  Stream<User?> get authStream => _authService.user;

  /// Method communicating with the authentication sign in method.
  Future<void> signIn(String email, String password) async {
    _user = await _authService.signIn(email, password);
    _errorMessage = _authService.errorMessage;
    notifyListeners();
  }

  /// Method communicating with the authentication sign off method.
  Future<void> signOut() async{
    await _authService.signOut();
    _user = null;
    notifyListeners();
  }
}