import 'package:flutter/cupertino.dart';

import 'package:athlon_task_atanas/models/user_model.dart';
import 'package:athlon_task_atanas/services/profile_service.dart';

/// State class for the profile data provider.
///
/// This class is taking the data from the ProfileService and passing it to the UI
/// via the provider pattern.
class ProfileState with ChangeNotifier{
  final service = ProfileService();
  User? profileData;

  /// Asynchronous method getting the profile data via the ProfileService.
  Future<void> getProfileData(String uid) async {
    final profileData = await service.getUserBy(uid);
    this.profileData = profileData;
    notifyListeners();
  }
}