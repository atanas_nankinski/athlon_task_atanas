import 'package:flutter/material.dart';

import 'package:athlon_task_atanas/ui/settings_page/profile_state_provider.dart';
import 'package:athlon_task_atanas/navigation/routing_constants.dart';
import 'package:athlon_task_atanas/ui/control_widget.dart';
import 'package:athlon_task_atanas/ui/login_screen.dart';
import 'package:athlon_task_atanas/ui/settings_page/settings_screen.dart';

/// Class holding the routing logic of the app.
class Routing {
  /// Static method used for pushing the routes that are being called from the ui.
  static Route<dynamic> generateRoute(RouteSettings settings){
    switch(settings.name){
      case RoutingConst.defaultRoute:
        return MaterialPageRoute(
          builder: (context) => const ControlWidget(),
          settings: settings,
        );
      case RoutingConst.loginRoute:
        return MaterialPageRoute(
          builder: (context) => const LoginScreen(),
          settings: settings,
        );
      case RoutingConst.viewAppRoute:
        return MaterialPageRoute(
          builder: (context) => const ProfileStateProvider(child: SettingsScreen()),
          settings: settings,
        );
      default:
        return MaterialPageRoute(
          builder: (context) => const ControlWidget(),
          settings: settings,
        );
    }
  }
}