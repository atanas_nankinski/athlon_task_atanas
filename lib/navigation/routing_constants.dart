/// Class holding static constant string values of the routes used in the app.
class RoutingConst {
  // Default route
  static const String defaultRoute = "/";

  // Route to the settings screen when user is not logged in
  static const String viewAppRoute = "view_app";

  // Route to the login screen
  static const String loginRoute = "login";
}