import 'package:firebase_auth/firebase_auth.dart' as auth;

import 'package:athlon_task_atanas/services/firebase-errors.dart';
import 'package:athlon_task_atanas/models/user_model.dart';

/// Class holding the authentication service
///
/// The authentication service is using firebase.
class AuthService{
  // Firebase instance
  final auth.FirebaseAuth _firebaseAuth = auth.FirebaseAuth.instance;
  // Error message that is later send to the provider for the ui
  String _errorMessage = "";

  /// Method later used for mapping the user after checking the authentication with firebase
  User? _userFromFirebase(auth.User? user){
    if(user == null){
      return null;
    }

    return User(uid: user.uid, email: user.email);
  }

  String get errorMessage => _errorMessage;

  /// Stream returning the user and its data
  ///
  /// if the user is null then the validation is not yet attempted.
  Stream<User?> get user{
    return _firebaseAuth.authStateChanges().map(_userFromFirebase);
  }

  /// Method holding the sign in logic
  ///
  /// The email and password are being passed to the method calling the firebase authentication
  /// if it throws exception then there was either something wrong with the values passed or
  /// there is no such user.
  Future<User?> signIn(String email, String password) async{
    try {
      final credential = await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);

      return _userFromFirebase(credential.user);
    } on auth.FirebaseAuthException catch(e){
      // Switch statement assigning the proper error message string.
      switch(e.code){
        case FirebaseErrors.userNotFound: _errorMessage = "Wrong email or password!";
          break;
        case FirebaseErrors.wrongPassword: _errorMessage = "Wrong email or password";
          break;
        case FirebaseErrors.invalidEmail: _errorMessage = "";
          break;
        default: _errorMessage = "Authentication failed";
      }
    } catch(e) {
      _errorMessage = "Error occurred!";
    }
  }

  /// Method calling the firebase sign out function.
  Future<void> signOut() async {
    return await _firebaseAuth.signOut();
  }
}