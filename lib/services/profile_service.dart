import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:athlon_task_atanas/models/user_model.dart';

/// Class holding the http service for the used data displayed in the settings screen.
class ProfileService {
  /// Method getting the user data.
  ///
  /// This method gets the user in the API service via uid passed from the firebase user data.
  Future<User?> getUserBy(String uid) async {
    final Uri testUrl = Uri.parse(AppConfig.endpoint + uid);
    final http.Response response = await http.get(testUrl);

    //Checking if the connection was successful by checking the returned status code
    if(response.statusCode >= 200 && response.statusCode < 300){
      final json = jsonDecode(response.body);

      if(json is List && json.isNotEmpty){
        final firstAvailable = json.first;

        return User.fromJson(firstAvailable);
      }else {
        return null;
      }
    }else {
      print("Something went wrong with the response");
      print(response.statusCode);
      return null;
    }
  }
}

/// Class holding configuration data for the http service
class AppConfig {
  // Endpoint for the API call
  static const String endpoint = "https://6214fb88cdb9d09717a8cbaf.mockapi.io/user_data?uid=";
}