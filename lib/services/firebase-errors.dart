/// Utility class holding const string values of firebase error codes for authentication
class FirebaseErrors {
  //Error code for when there is no such user in the firebase
  static const String userNotFound = "user-not-found";

  //Error code if the user was found the password is wrong
  static const String wrongPassword = "wrong-password";

  //Error code if the email send to the firebase is in the wrong email format
  static const String invalidEmail = "invalid-email";
}