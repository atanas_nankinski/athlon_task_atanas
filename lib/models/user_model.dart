/// Model of the user and its data that is being taken from the services
///
/// This model is used by both the AuthenticationService and the ProfileService.
class User {
  final String? uid;
  final String? email;
  final String? avatarUrl;
  final String? updatedAt;

  User({this.uid, this.email, this.avatarUrl, this.updatedAt});
  User.fromJson(Map<String, dynamic> json) :
        uid = json["uid"],
        email = json["userEmail"],
        avatarUrl = json["avatar"],
        updatedAt = json["updatedAt"];
}