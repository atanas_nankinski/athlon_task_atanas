# athlon_task_atanas

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

### Testing user credentials

The predefined user profiles in the firebase and MockApi are:
    user 1:
        email: test1@test.com
        password: 123456789
    user 2:
        email: test2@test.com
        password: 123456789
